import React from 'react'
import Navbar from '../parts/Navbar'
import Hero from '../parts/Hero'
import Treat from '../parts/Treat'
import Convo from '../parts/Convo'
import Story from '../parts/Story'
import Community from '../parts/Community'
import Sitemap from '../parts/Sitemap'
import Footer from '../parts/Footer'

export default function Homepage() {
  return (
    <>
    <div className='bg-white'>

        <Navbar />
        <Hero />
        <Treat />
        <Convo />
        <Story />
        <Community />
        <Sitemap />
        <Footer />
    </div>
    </>
  )
}
