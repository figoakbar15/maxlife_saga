import React, { useState } from 'react';

export default function Navbar() {
  
    const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="flex items-center justify-between pt-12 px-10 bg-transparent md:bg-gray-200 ">
      <div className="flex items-center ml-12">
        <button
          onClick={toggleMenu}
          className="text-white focus:outline-none focus:ring-2 focus:ring-white mr-4 sm:hidden"
        >
        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M29 20H5" stroke="#231F20" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M32 9L5 9" stroke="#231F20" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M35 31H5" stroke="#231F20" stroke-width="5" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>

        </button>
        <img src="/images/Logo_MAXLIFE.svg" alt="Logo" className="mr-2" />
      </div>
      <div className="md:hidden">
        {isOpen && (
          <ul className="mt-4 space-y-2">
           <li>
          <a href="#" className="text-black font-semibold">Max for Dogs</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold">Max for Cats</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold">Forum</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold">Vet Talk</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold">Max Share</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold">Contact Us</a>
        </li>
        <li>
        <button className="bg-blue-500 text-white py-4 px-8 rounded-xl mr-12">Join Us</button>
        </li>
          </ul>
        )}
      </div>
      <div className="hidden sm:block">

      <ul className={`sm:flex sm:space-x-10 mt-4 sm:mt-0`}>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Max for Dogs</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Max for Cats</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Forum</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Vet Talk</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Max Share</a>
        </li>
        <li>
          <a href="#" className="text-black font-semibold font-raleway">Contact Us</a>
        </li>
      </ul>
      </div>
      <button className="bg-blue-500 text-white py-4 px-8 rounded-xl mr-12 hidden sm:block font-raleway">Join Us</button>
    </nav>
  )
}
