import React from 'react'

export default function Footer() {
  return (
    <footer className="container mx-auto text-center px-4 py-8 justify-center">
            <div className="container mx-auto flex justify-center">
                <a href="#" className="mr-4">
                <img src="images/Facebook.png" alt="Logo 1" className="h-auto" />
                </a>
                <a href="#" className="mr-4">
                <img src="images/Twitter.png" alt="Logo 2" className="h-auto" />
                </a>
                <a href="#" className="mr-4">
                <img src="images/Instagram.png" alt="Logo 3" className="h-auto" />
                </a>
                <a href="#">
                <img src="images/Youtube.png" alt="Logo 4" className="h-auto" />
                </a>
        </div>
      <p className="font-bold font-raleway md:text-xl text-md text-gray-500 mt-8">
            © 2021 Maxlife. All Rights Reserved.
      </p>
    </footer>
  )
}
