import React from 'react'

export default function Treat() {
  return (
    <section className="relative">
        <div className="grid grid-cols-5 gap-4">
            <div className="col-span-1 justify-start">
                <img src="images/DogTreat.png" alt="Image 1" className="absolute w-1/4 md:w-auto" />
            </div>
            <div className="col-span-3 flex flex-col justify-center items-center">
            <h1 className="text-2xl md:text-6xl text-gray-800 font-extrabold 
                    font-raleway text-center md:text-center md:mt-20 md:pt-16 mt-14" 
                    >
                    Treat They
                    <span className="bg-yellow-500 rounded-xl py-2 px-2">Deserve</span> 
                </h1>
                
            </div>
            <div className="col-span-1 flex justify-end">
                <img src="images/CatTreat.png" alt="Image 2" className="absolute w-1/5 md:w-auto top-0 right-0" />
            </div>
        </div>
        <p className="text-gray-600 font-raleway md:mx-0 mx-5 text-sm md:text-2xl text-center md:text-center mt-6 md:mb-0 mb-8" style={{ lineHeight: '2' }}>
                    They deserve healthy food just like you. Not just an <br />ordinary food
                    for pet tummy, but it is a healthy, and no allergic thread
        </p>

        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 md:mt-56 mt-0">
            <div className="flex flex-col items-center md:mb-16 mb-0">
                <img src="images/FinestIngredient.png" alt="Image 1" className="w-auto " />
                <h3 className="text-center mt-2 text-blue-400 font-raleway font-bold md:text-2xl text-lg">Finest Ingredient</h3>
                <p className="text-center font-raleway font-medium mt-2 md:text-lg text-md text-gray-600">Perfectly Finest since harvest <br />until packaged</p>
            </div>
            
            <div className="flex flex-col items-center md:mt-16 mt-0">
                <img src="images/RealFood.png" alt="Image 2" className="w-auto" />
                <h3 className="text-center mt-2 text-blue-400 font-raleway font-bold md:text-2xl text-lg">Real Seafood</h3>
                <p className="text-center font-raleway font-medium mt-2 md:text-lg text-md text-gray-600">Made from the sea to the <br />land, no bad sea food</p>
            </div>
            <div className="flex flex-col items-center md:mb-16 mb-0">
                <img src="images/TrustedSupplier.png" alt="Image 3" className="w-auto" />
                <h3 className="text-center mt-2 text-blue-400 font-raleway font-bold md:text-2xl text-lg">Trusted Supplier</h3>
                <p className="text-center font-raleway font-medium mt-2 md:text-lg text-md text-gray-600">Fresh fom the farm since <br /> packed to pet tummy</p>
            </div>
            <div className="flex flex-col items-center md:mt-16 mt-0">
                <img src="images/LifetimeFood.png" alt="Image 4" className="w-auto" />
                <h3 className="text-center mt-2 text-blue-400 font-raleway font-bold md:text-2xl text-lg">A lifetime Food</h3>
                <p className="text-center font-raleway font-medium mt-2 md:text-lg text-md text-gray-600">From newborn until senior, <br />spanning pet life span.</p>
            </div>
        </div>
    </section>
  )
}
