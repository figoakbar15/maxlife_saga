import React from 'react'

export default function Story() {
  return (
    <section className='relative bg-white'>
        <div className="absolute top-0 left-0 w-full h-full z-0">
            <div className=" bg-[url('images/Storybg1_1.png')] md:bg-[url('images/Storybg1.png')]
            bg-cover bg-right-top h-full md:-mt-64 -mt-64" 
                style={{backgroundSize: '20% auto',
                backgroundRepeat: 'no-repeat'}}>
            <div className="bg-cover bg-left h-full w-1/2 mt-64" 
            style={{ backgroundImage: 'url(images/Storybg2.png)',
                    backgroundSize: '45% auto',
                    backgroundRepeat: 'no-repeat' }}>
                    
            </div>
            </div>
           
        </div>
         
        <div className='relative container mx-auto mt-32 '>
            <h1 className='md:mb-16 mb-2 font-raleway text-3xl md:text-5xl text-gray-800 font-extrabold text-center md:text-start'>
                <span className='bg-yellow-500 rounded-xl py-2 px-2'>Story</span> & Highlights
            </h1>
                <div className=" grid grid-cols-1 md:grid-cols-3 flex">
                        <div className="flex flex-col col-span-2 justify-center items-center my-5" >
                            <img
                                src='images/Storycard1.png'
                                alt="Image"
                                className="md:w-full w-11/12 h-auto rounded-3xl"
                                />
                        </div> 
                        <div className="card flex justify-center items-center col-span-1">
                            <div className="bg-gray-200 p-4 relative rounded-3xl py-14 md:-ml-32 ml:5 md:w-auto w-11/12">
                                <h1 className='font-raleway font-bold md:text-4xl text-2xl mx-5'>Here’s How Dog & Cat serve their four legged Child in Pandemic</h1>
                                <p className="my-8 mx-5 text-gray-600 font-raleway font-medium md:text-md text-sm" >Exact composition of dog food varies widely from one manufacturer
                                to another, dog food generally is made up of meats, meat byproducts, cereals, grains and vitamins.</p>
                                <p className="absolute bottom-0 left-0 mb-8 ml-10 font-raleway font-bold text-gray-700">June 15, 2023</p>
                                <a href="#" className="absolute bottom-0 right-0 mb-8 mr-14 font-raleway font-bold text-blue-500">Read more</a>
                            </div>
                        </div>
                </div>
            <div className="relative grid grid-cols-1 md:grid-cols-3 flex md:gap-3 gap-0">
                    <div className="flex flex-col col-span-1 my-5 md:items-start items-center" >
                        <img 
                            src="images/Storycard2.png"
                            className='rounded-3xl md:w-full w-11/12'
                        />
                        <h1 className='font-raleway font-bold md:text-2xl text-xl mr-5 my-5 md:ml-0 ml-5'>Here’s How Dog & Cat serve their four legged Child in Pandemic</h1>
                        <p className="bottom-0 left-0 font-raleway font-bold text-gray-700">June 15, 2023</p>
                    </div>  
                    <div className="flex flex-col col-span-1 my-5 md:justify-start md:items-start justify-center items-center" >
                        <img 
                            src="images/Storycard4.png"
                            className='rounded-3xl md:w-full w-11/12'
                        />
                        <h1 className='font-raleway font-bold md:text-2xl text-xl mr-5 my-5 md:ml-0 ml-5'>Here’s How Dog & Cat serve their four legged Child in Pandemic</h1>
                        <p className="bottom-0 left-0 font-raleway font-bold text-gray-700 md:text-start text-center">June 15, 2023</p>
                    </div>  
                    <div className="flex flex-col col-span-1 my-5 md:justify-start md:items-start justify-center items-center" >
                        <img 
                            src="images/Storycard3.png"
                            className='rounded-3xl md:w-full w-11/12'
                        />
                        <h1 className='font-raleway font-bold md:text-2xl text-xl mr-5 my-5 md:ml-0 ml-5'>Here’s How Dog & Cat serve their four legged Child in Pandemic</h1>
                        <p className="bottom-0 left-0 font-raleway font-bold text-gray-700 md:text-start text-center">June 15, 2023</p>
                    </div>  
            </div>
                <div className="flex justify-center items-center md:mt-10 mt-0">
                    <button className="bg-blue-500 text-lg text-white font-bold md:py-5 md:px-24 py-4 px-16 rounded-xl font-raleway font-bold z-10">See All Story</button>
                </div>
        </div>

       
    </section>
  )
}
