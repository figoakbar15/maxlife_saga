import React from 'react'

export default function Community() {
  return (
    <section className="relative md:mt-64 mt-20 md:pb-64 md:pb-32 pb-0">
        <h1 className="text-2xl md:text-6xl text-gray-800 font-extrabold 
                    font-raleway text-center md:text-center md:mt-20 top-0" 
                    >
                    Dog & Cat 
                    <span className="bg-yellow-500 rounded-xl py-2 px-2">Community</span> 
                </h1>
                <p className="text-gray-600 font-raleway md:mx-0 mx-5 text-sm md:text-2xl text-center md:text-center mt-6 md:mb-0 mb-8" style={{ lineHeight: '2' }}>
                    We’re happy to announce that these four legged fella is deserve same <br />
                    treatment as us, be one of us
                </p>
                <div className="flex justify-center items-center md:mt-10 mt-0">
                    <button className="bg-blue-500 text-lg text-white font-bold md:py-5 md:px-24 py-4 px-6 rounded-xl font-raleway font-bold z-10">
                        Join Community
                    </button>
                </div>
         <div className="absolute top-0 left-0 w-full h-full z-0 grid grid-cols-1 md:grid-cols-2 gap-10 mb-16 ">
            <div className="col-span-1 justify-start md:mt-0 mt-32">
                <img src="images/CatCommunity.png" alt="Image 1" className="w-1/4 md:w-auto" />
            </div>
            <div className="col-span-1 flex justify-end">
                <img src="images/DogCommunity.png" alt="Image 2" className="absolute w-1/4 md:w-auto top-0 right-0 md:-mt-32 mt-28" />
            </div>
        </div>   
        <div className=" grid grid-cols-1 md:grid-cols-2 flex mt-16 md:gap-10 gap-0">
                        <div className="flex flex-col col-span-1 flex justify-end md:items-end items-center" >
                            <img
                                src='images/Community1.png'
                                alt="Image"
                                className="md:w-auto w-11/12 h-auto rounded-3xl "
                                />
                        </div> 
                        <div className="flex col-span-1 flex justify-start items-center">
                        <img
                                src='images/Community2.png'
                                alt="Image"
                                className="md:w-auto w-11/12 h-auto rounded-3xl"
                                />
                        </div>
                </div>
                <div className="relative grid grid-cols-1 md:grid-cols-3 flex md:gap-3 gap-0 z-10">
                    <div className="flex flex-col col-span-1 justify-center items-end md:-mr-28 -mt-0 z-10" >
                        <img 
                            src="images/Community3.png"
                            className='rounded-3xl md:w-auto w-11/12 h-auto rounded-3xl'
                        />
                    </div>
                    <div className="flex flex-col col-span-1 md:items-end items-center z-0" >
                        <img 
                            src="images/Community4.png"
                            className='rounded-3xl md:w-auto w-auto'
                        />
                    </div>
                    <div className="flex flex-col col-span-1 md:items-center md:items-center justify-center items-end z-0 mr-5 md:mr-0">
                        <img 
                            src="images/Community5.png"
                            className='rounded-3xl md:w-auto w-10/12'
                        />
                    </div>

            </div>
            <div className="absolute bottom-0 w-full h-full z-0 grid grid-cols-2">
                <div className="flex flex-col col-span-1 bg-cover bg-left-bottom h-full w-1/2 
                bg-[url('images/Communitybg1-1.png')] md:bg-[url('images/CommunityBg1.png')]" 
                style={{ 
                        backgroundSize: '65% auto',
                        backgroundRepeat: 'no-repeat' }}>
                        
                </div>
                <div className=" flex flex-col col-span-1 bg-contain md:bg-right bg-right-bottom h-auto w-auto md:w-full 
                bg-[url('images/CommunityBg2.png')]" 
                style={{ 
                        backgroundSize: '25% auto',
                        backgroundRepeat: 'no-repeat' }}>
                        
                </div>
        </div>
        
    </section>
  )
}
