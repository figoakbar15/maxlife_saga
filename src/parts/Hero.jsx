import React from 'react'

export default function Hero() {
  return (
    <section className="relative bg-white">
      <div
        className=" static top-0 left-0 right-0 h-auto bg-gray-200 md:pt-16 pt-0 z-0"
        style={{ clipPath: 'ellipse(100% 100% at 50% 0%)' }}
      >
        <div className="absolute top-0 left-0 w-full h-full z-0 grid grid-cols-1 md:grid-cols-2 gap-10 mb-16 ">
            <div className="bg-cover bg-left-top h-full w-1/2 invisible md:visible" 
            style={{ backgroundImage: 'url(images/BackgroundHero1.png)',
                    backgroundSize: '65% auto',
                    backgroundRepeat: 'no-repeat' }}>
                    
            </div>
            <div className="bg-cover bg-right-top h-full" 
            style={{ backgroundImage: 'url(images/BackgroundHero2.png)',
                    backgroundSize: '80% auto',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'top right' }}>
                    
            </div>
        </div>
        <div className="container mx-auto px-4 relative mt-10">
            <div className="grid grid-cols-1 md:grid-cols-2 gap-10">
                <div className="ml-0 md:ml-5">
                    <div className="flex flex-col justify-center my-5" >
                        <h1 className="text-3xl md:text-6xl font-extrabold font-raleway mb-3 text-center md:text-start text-gray-800" 
                        style={{ lineHeight: '1.5' }}>
                            Everything
                        <br/>Your <span className="bg-yellow-500 rounded-xl py-2 px-2">Companion</span> 
                            <br/>Need The Most</h1>
                            <p className="text-gray-600 mx-3 font-raleway md:mx-0 text-lg md:text-xl text-center md:text-start">Perfect thread & good care for perfect companion, is 
                            all you gave for your four legged angel</p>
                    </div>
                    <div className="flex justify-center items-center md:justify-start md:mt-10 mt-0">
                        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-6 rounded-xl font-raleway font-bold z-10">Get Free Sample</button>
                    </div>
                </div>
                <div>
                    <img
                        src='images/HeroImage.png'
                        alt="Image"
                        className="w-full h-auto relative"
                    />
                </div>
            </div>
        </div>
      </div>
      <div className=" relative mx-auto flex md:justify-center md:items-center md:w-full md:gap-7 gap-4 z-10 md:-mt-24 -mt-16 md:mx-0 mx-6 md:mb-0 mb-6">
        <div className="bg-purple-400 shadow-md rounded-[20px] md:rounded-[36px] pr-6 flex">
            <div className="flex-shrink-0 pt-5 w-1/2 md:w-auto">
            <img src="images/Dog.png" alt="Card Image" className="h-full w-full" />
            </div>
            <div className="md:mr-5 flex justify-center items-center">
            <h2 className="text-sm font-bold font-raleway text-white md:text-3xl">I'm a Dog <br />Person</h2>
            </div>
        </div>
        <div className="bg-blue-400 shadow-md rounded-[20px] md:rounded-[36px] pr-6 flex">
            <div className="flex-shrink-0 pt-5 w-1/2 md:w-auto">
            <img src="images/Cat.png" alt="Card Image" className="h-full w-full" />
            </div>
            <div className="md:mr-5 flex justify-center items-center">
            <h2 className="text-sm font-bold font-raleway text-white md:text-3xl">I'm a Cat <br />Person</h2>
            </div>
        </div>
        </div>
    </section>
  )
}

