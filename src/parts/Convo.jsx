import React from 'react'

export default function Convo() {
  return (
    <section className='relative md:mt-40 mt-10'>
        <div className=" relative grid grid-cols-1 lg:grid-cols-2 bg-gray-200">
            <div className="container mx-auto flex flex-col justify-center align-center bg-gray-200">
                <div className=' items-center md:justify-start md:my-0 my-10 mx-auto'>
                    <h2 className="md:text-6xl text-2xl font-extrabold text-start font-raleway text-gray-800" 
                    style={{ lineHeight: '1.5' }}>Need 24 hrs <span className='bg-yellow-500 rounded-xl py-2 px-2'>Convo</span> to <br />take care of your Pet?</h2>
                    <div className="flex justify-center items-start md:justify-start">
                        <button className="bg-blue-500 text-white font-bold py-4 px-6 my-6 rounded-xl font-raleway font-bold">Begin Vet Talk</button>
                    </div>
                </div>
            </div>
            <div className="relative flex justify-center items-center w-full h-full bg-cover py-24
            bg-[url('images/Mobilebg.png')] md:bg-[url('images/ConvoBg.png')]">       
                <img src="images/Cat&Dog.png" alt="Image" className="absolute max-w-auto max-h-auto z-10 md:-ml-56 -ml-10" />
                <img src="images/M_white.png" alt="Image" className=" max-w-auto max-h-auto z-0 -ml-56 hidden md:block" />
            </div>
        </div>
    </section>
  )
}
