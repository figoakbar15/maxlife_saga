import React from 'react'

export default function Sitemap() {
  return (
    <section className="sitemap">
      <div className="border-b border-t border-black py-12 px-4 md:mt-0 mt-5">
        <div className="flex justify-center mb-8">
          <img
            src="/images/Logo_MAXLIFE.svg"
            alt="Images"
          />
        </div>
        <footer className="py-4">
            <div className="container mx-auto grid md:grid-cols-6 grid-cols-2 md:gap-4 gap-10 md:text-start text-center">
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">Maxlife For Dog</a>
                    <p className="text-black font-medium font-raleway mt-4">Vet Talk</p>
                    <p className="text-black font-medium font-raleway mt-4">Max Pet Life</p>
                    <p className="text-black font-medium font-raleway mt-4">Max Share</p>
                    <p className="text-black font-medium font-raleway mt-4">Max In Love</p>
                </div>
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">Maxlife For Cat</a>
                    <p className="text-black font-medium font-raleway mt-4">Vet Talk</p>
                    <p className="text-black font-medium font-raleway mt-4">Max Pet Life</p>
                    <p className="text-black font-medium font-raleway mt-4">Max Share</p>
                    <p className="text-black font-medium font-raleway mt-4">Max In Love</p>
                </div>
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">Shop</a>
                    <p className="text-black font-medium font-raleway mt-4">Shop For Dogs</p>
                    <p className="text-black font-medium font-raleway mt-4">Shop For Cats</p>
                </div>
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">News & Story</a>
                </div>
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">About Us</a>
                    <p className="text-black font-medium font-raleway mt-4">Our History</p>
                    <p className="text-black font-medium font-raleway mt-4">Our Team</p>
                    <p className="text-black font-medium font-raleway mt-4">FAQ</p>
                </div>
                <div className="col-span-1">
                    <a href="#" className="text-black font-extrabold font-raleway">Contact Us</a>
                </div>
            </div>
        </footer>
      </div>
    </section>
  )
}
